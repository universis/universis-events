import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { HomeRoutingModule } from './home-routing.module';
import {HomeService} from './services/home.service';
import {UserService} from './services/user.service';
import {TranslateModule, TranslateService} from '@ngx-translate/core';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    HomeRoutingModule,
    TranslateModule
  ],
  providers: [
    HomeService,
    UserService,
    TranslateService
  ],
})
export class HomeModule { }
