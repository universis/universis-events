import { Component, OnInit } from '@angular/core';
import {HomeService} from '../../services/home.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private userService: UserService,
              private homeService: HomeService) { }

  ngOnInit(): void {

  }

  public async getUser() {
    const instructor = await this.userService.getInstructor();
    console.log(instructor);
  }

  public async createNewEvent() {
    return await this.homeService.createEvent();
  }
  public async getEvents() {
    console.log(await this.homeService.getEvents());
  }
  public async getClassEvents() {
    console.log(await this.homeService.getClassEvents());
  }
}
