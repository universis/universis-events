import { Component, OnInit } from '@angular/core';
import { ModalService } from '@universis/common';
import { AdvancedSelectComponent } from '@universis/tables';
import { AdvancedSelectService } from '@universis/tables';

@Component({
  selector: 'app-instructor-events',
  templateUrl: './instructor-events.component.html'
})
export class InstructorEventsComponent implements OnInit {

  public selected: Array<any> = [];

  constructor(private _modal: ModalService, private _selectService: AdvancedSelectService) { }

  ngOnInit() {
  }

  selectStudent() {
    this._selectService.select({
      modalTitle: 'Select student',
      tableConfigSrc: '/assets/lists/TeachingEvents/instructor.json'
    }).then((result) => {
      if (result.result === 'ok') {
        this.selected = result.items;
      } else {
        this.selected = [];
      }
    });
  }

}
