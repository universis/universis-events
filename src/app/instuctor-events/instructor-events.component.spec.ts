import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorEventsComponent } from './instructor-events.component';

describe('InstuctorEventsComponent', () => {
  let component: InstructorEventsComponent;
  let fixture: ComponentFixture<InstructorEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
