export const EVENTS_CONFIGURATION = {
  "title": "Events",
  "model": "instructors/me/teachingEvents",
  "searchExpression":"indexof(name, '${text}') ge 0",
  "selectable": false,
  "multipleSelect": false,
  "columns": [
    {
      "name":"startDate",
      "property": "startDate",
      "formatter": "DateFormatter",
      "title": "Date"
    },
    {
      "name":"endDate",
      "property": "endDate",
      "hidden": true
    },
    {
      "name":"courseClass/title",
      "property": "courseClass",
      "title": "Class",
      "hidden": false
    },
    {
      "name": "id",
          "formatter": "ButtonFormatter",
          "formatString": "#/universis-events/${id}",
          "className": "text-center",
          "formatOptions": {
            "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
            "buttonClass": "btn btn-default"
          }
    }
  ],
  "criteria": [
    {
      "name": "courseClass",
      "filter": "(indexof(courseClass/name, '${value}') ge 0)",
      "type": "text"
    },  ],
  "searches": [  ],
  "paths" : [  ]
}
